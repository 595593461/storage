package com.example.storage.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.web.multipart.MultipartFile;

import java.util.*;

/*
    此配置类不需要改动
*/
@Configuration// 包含@Component
@PropertySource(value = {"classpath:application.properties"}, encoding = "utf-8")
@ConfigurationProperties(prefix = "hdfs")
public class HdfsConfig {
    // @Value("${hdfs.URL}")
    private String URL;
    private String owner; // (linux里配置hadoop的用户名)
    private int bufferSize;
    private String tempDir;
    public static final String basicDir = "/bucket"; // 根目录
    public static final String noEncryptionDir = "/noEncryption";
    public static final String encryptionDir = "/encryption";
    public static final String secretKeyDir = "/secretKey";
    public static int dirContainFilesNum = 100000;
    public static Map<Long, MultipartFile> idBindFile = new HashMap<>();
    public static Map<Long, String> idBindDir = new HashMap<>();
    public static Map<Long, String> idBindPath = new HashMap<>();
    public static Map<String, String> publicKeyBindPrivateKey = new HashMap<>();

    public String getURL() {
        return URL;
    }

    public void setURL(String URL) {
        this.URL = URL;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public int getBufferSize() {
        return bufferSize;
    }

    public void setBufferSize(int bufferSize) {
        this.bufferSize = bufferSize;
    }

    public String getTempDir() {
        return tempDir;
    }

    public void setTempDir(String tempDir) {
        this.tempDir = tempDir;
    }
}