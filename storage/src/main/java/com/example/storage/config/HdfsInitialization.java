package com.example.storage.config;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;

import java.net.URI;

public class HdfsInitialization {

    private static Configuration getConfiguration(String URL) {
        Configuration configuration = new Configuration();
        configuration.set("fs.defaultFS", URL);
        return configuration;
    }

    public static FileSystem getFileSystem(String URL, String owner) throws Exception {
        // 初始化Hdfs文件系统
        FileSystem fileSystem = FileSystem.get(new URI(URL), getConfiguration(URL), owner);
        return fileSystem;
    }
}