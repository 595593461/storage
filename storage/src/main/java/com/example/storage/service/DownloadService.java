package com.example.storage.service;

import com.example.storage.config.HdfsConfig;
import com.example.storage.config.HdfsInitialization;
import org.apache.commons.lang3.StringUtils;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

@Service
public class DownloadService {

    public boolean download(String URL, String owner, long id, String downloadPath) throws Exception {

        MultipartFile multipartFile = HdfsConfig.idBindFile.get(id);
        String key = multipartFile.getOriginalFilename();
        FileSystem fs = HdfsInitialization.getFileSystem(URL, owner);
        String targetPath = HdfsConfig.idBindPath.get(id);
        // 重命名，恢复原文件名
        int index = key.indexOf("}");
        String str = key.substring(index + 1);
        String newName = HdfsConfig.idBindDir.get(id) + "/" + str;
        FileRename.fileRename(URL, owner, targetPath, newName);

        if (StringUtils.isEmpty(newName) || StringUtils.isEmpty(downloadPath)) {
            return false;
        }

        Path clientPath = new Path(newName);
        Path serverPath = new Path(downloadPath);
        // 调用文件系统的文件复制方法，第一个参数是否删除原文件true为删除，默认为false
        if (!fs.exists(new Path(newName)))
            throw new IllegalArgumentException("文件不存在！");
        fs.copyToLocalFile(false, clientPath, serverPath, true);
        fs.close();
        // 重命名，下载之后名字恢复
        boolean isdownload =  FileRename.fileRename(URL, owner, newName, targetPath);
        return isdownload;
    }
}