package com.example.storage.service.mysqlservice;

import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Date;

@Service
public class FileTimeService {
    public String fileTime(long id) {

        SimpleDateFormat formatter = new SimpleDateFormat("yyyy年-MM月dd日-HH时mm分ss秒");
        Date date = new Date(id);
        return formatter.format(date);
    }
}
