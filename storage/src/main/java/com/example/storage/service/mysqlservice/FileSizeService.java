package com.example.storage.service.mysqlservice;

import com.example.storage.config.HdfsInitialization;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.LocatedFileStatus;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.fs.RemoteIterator;
import org.springframework.stereotype.Service;

@Service
public class FileSizeService {
    public long fileSize(String URL, String owner, String targetPath) throws Exception {

        FileSystem fs = HdfsInitialization.getFileSystem(URL, owner);
        Path filePath = new Path(targetPath);
        RemoteIterator<LocatedFileStatus> iterator = fs.listFiles(filePath, false);
        long size = 0;
        while (iterator.hasNext()) {
            long temp = iterator.next().getLen(); // getBlockSize();
            size += temp;
        }
        return size;
    }
}
