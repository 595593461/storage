package com.example.storage.service;

import com.example.storage.config.HdfsInitialization;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.springframework.stereotype.Service;

@Service
public class MkdirService {

    public static int count = 0;

    public static boolean mkdir(String URL, String owner, String directory, String groupname) throws Exception {
        // directory为文件夹路径(相对路径)
        FileSystem fileSystem = HdfsInitialization.getFileSystem(URL, owner);
        Path directoryPath = new Path(directory);
        if (!fileSystem.exists(directoryPath)) {
            // 创建文件夹
            fileSystem.mkdirs(directoryPath);
            // 设置用户名和组名
            fileSystem.setOwner(directoryPath, owner, groupname);
            count++;
            return true;
        } else
            return false;
    }
}