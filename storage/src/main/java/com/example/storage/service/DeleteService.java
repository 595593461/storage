package com.example.storage.service;

import com.example.storage.config.HdfsConfig;
import com.example.storage.config.HdfsInitialization;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.springframework.stereotype.Service;

@Service
public class DeleteService {

    public boolean delete(String URL, String owner, Long id) throws Exception {

        FileSystem fs = HdfsInitialization.getFileSystem(URL, owner);
        String deletePath = HdfsConfig.idBindPath.get(id);
        if (!fs.exists(new Path(deletePath)))
            throw new IllegalArgumentException("文件不存在！");
        boolean isDelete = fs.deleteOnExit(new Path(deletePath));
        fs.close();
        return isDelete;
    }

}