package com.example.storage.service.mysqlservice;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

@Service
public class CreateTableService {
    public void creatStorageTable(JdbcTemplate jdbcTemplate) {
        String sql = "CREATE TABLE storage" +
                "(" +
                " id VARCHAR(30) PRIMARY KEY," +
                " filesize VARCHAR(30)," +
                " filetime VARCHAR(30)" +
                ")";
        jdbcTemplate.update(sql);
    }

    public void creatSecretKeyTable(JdbcTemplate jdbcTemplate) {
        String sql = "CREATE TABLE secretkey" +
                "(" +
                "  publicKey VARCHAR(1024)," +
                "  privateKey VARCHAR(1024)" +
                ")";
        jdbcTemplate.update(sql);
    }

}
