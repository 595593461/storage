package com.example.storage.service;

import com.example.storage.config.HdfsInitialization;
import org.apache.hadoop.fs.*;
import org.springframework.stereotype.Service;

@Service
public class StatisticsService {

    public static int dirContainFilesCount = 0;

    public int fileNumber(String URL, String owner, String targetDir) throws Exception {
        FileSystem fs = HdfsInitialization.getFileSystem(URL, owner);
        RemoteIterator<LocatedFileStatus> files = fs.listFiles(new Path(targetDir), false);
        int dirContainFilesCount = 0;
        while (files.hasNext()) {
            files.next();
            dirContainFilesCount++;
            StatisticsService.dirContainFilesCount = dirContainFilesCount;
        }
        fs.close();
        return dirContainFilesCount;
    }

}