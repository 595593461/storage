package com.example.storage.service;

import com.example.storage.config.HdfsInitialization;
import org.apache.commons.lang3.StringUtils;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;

public class FileRename {

    public static boolean fileRename(String URL, String owner, String oldName, String newName) throws Exception {

        if (StringUtils.isEmpty(oldName) || StringUtils.isEmpty(newName)) {
            return false;
        }
        FileSystem fs = HdfsInitialization.getFileSystem(URL, owner);
        // 原文件目标路径
        Path oldPath = new Path(oldName);
        // 重命名目标路径
        Path newPath = new Path(newName);
        boolean isSuccess = fs.rename(oldPath, newPath);
        fs.close();
        return isSuccess;
    }

}
