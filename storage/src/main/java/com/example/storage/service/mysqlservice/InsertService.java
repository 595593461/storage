package com.example.storage.service.mysqlservice;

import com.example.storage.config.HdfsConfig;
import com.example.storage.service.CreateSecretKey;
import com.example.storage.service.UploadService;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import java.util.Map;

@Service
public class InsertService {
    public void insertToStorage(JdbcTemplate jdbcTemplate) {

        String ID = UploadService.ID;
        String filesize = UploadService.size;
        String filetime = UploadService.time;
        String sql = "insert into storage(id,filesize,filetime) values('" + ID + "','" + filesize + "','" + filetime + "')";
        jdbcTemplate.update(sql);
    }

    public void insertToKey(JdbcTemplate jdbcTemplate) {

        Map<String, Object> keyMap;
        try {
            keyMap = CreateSecretKey.initKey();
            String publicKey = CreateSecretKey.getPublicKey(keyMap);
            String privateKey = CreateSecretKey.getPrivateKey(keyMap);
            //绑定公钥私钥
            HdfsConfig.publicKeyBindPrivateKey.put(publicKey,privateKey);
            String sql = "insert into secretkey (publicKey,privateKey) values ('" + publicKey +  "','" + privateKey + "')";
            jdbcTemplate.update(sql);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
