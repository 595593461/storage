package com.example.storage.service;

import com.example.storage.config.HdfsConfig;
import com.example.storage.config.HdfsInitialization;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IOUtils;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import com.example.storage.service.mysqlservice.*;

import java.io.*;

@Service
public class UploadService implements UploadInterface {
    // 设置hadoop里文件夹对应编号
    private static int noEncyptFolderNumber = 1;
    private static int encryptFolderNumber = 1;

    public static String ID;
    public static String size;
    public static String time;

    // hadoop里文件的唯一标识-ID
    private long id = System.currentTimeMillis();

    @Override
    public void idBindFile(long id, MultipartFile file) {
        HdfsConfig.idBindFile.put(id, file);
    }

    @Override
    public void idBindDir(long id, String targetDir) {
        HdfsConfig.idBindDir.put(id, targetDir);
    }

    @Override
    public void idBindPath(long id, String targetPath) {
        HdfsConfig.idBindPath.put(id, targetPath);
    }

    @Override
    public void createParentDir(String URL, String owner) throws Exception {
        // 在hadoop里创建一个根目录
        if (MkdirService.count < 1)
            MkdirService.mkdir(URL, owner, HdfsConfig.basicDir, "supergroup");
    }

    @Override
    public File createTempLocalDir(MultipartFile file, String tempDir) {
        // 创建本地临时文件夹
        File instance = new File(tempDir);
        if (!instance.exists())
            instance.mkdir();
        return instance;
    }

    @Override
    public File tempUploadToLocal(MultipartFile file, String tempDir) throws IOException {
        // 将表单上传的文件暂时上传至本地临时文件夹内
        String tempPath = tempDir + "/" + file.getOriginalFilename();
        File tempLocalPath = new File(tempPath);
        file.transferTo(tempLocalPath);
        return tempLocalPath;
    }

    @Override
    public void deleteTempDir(File tempDir) {
        // 删除本地临时目录
        while (tempDir.exists() && tempDir.isDirectory()) {
            if (tempDir.exists() && tempDir.isDirectory()) {
                File[] tempFile = tempDir.listFiles();
                for (int i = 0; i < tempFile.length; i++) {
                    tempFile[i].delete();
                }
            }
            tempDir.delete();
        }
    }

    @Override
    public int autoCreateDir(String URL, String owner, String targetDir, boolean isEncypt) throws Exception {
        // 在hadoop里上传文件时，自动创建文件夹，且每个文件夹可存放文件数为dirContainFilesNum
        int fileCount = new StatisticsService().fileNumber(URL, owner, targetDir);
        if (fileCount >= HdfsConfig.dirContainFilesNum) {
            if (!isEncypt)
                noEncyptFolderNumber++;
            else
                encryptFolderNumber++;
            StatisticsService.dirContainFilesCount = 0;
        }
        return fileCount;
    }

    @Override
    public void hadoopUpload(String URL, String owner, int bufferSize, File tempPath, String targetPath) throws Exception {
        // 初始化文件系统
        FileSystem fileSystem = HdfsInitialization.getFileSystem(URL, owner);
        // 创建上传文件的输入流
        InputStream in = new FileInputStream(tempPath.getAbsoluteFile());
        // 创建目标文件的输出流
        OutputStream out = fileSystem.create(new Path(targetPath));
        // 使用Hadoop提供的IOUtils,将in的内容copy到out,设置buffSize大小,并将该流关闭
        IOUtils.copyBytes(in, out, bufferSize, true);
        in.close();
        out.close();
    }

    public long upload(String URL, String owner, int bufferSize, MultipartFile file, boolean isEncrypt, String tempDirArg) throws Exception {
        // ID与文件绑定
        idBindFile(id, file);
        // 创建本地临时文件夹，并暂存上传的文件
        createParentDir(URL, owner);
        File tempDir = createTempLocalDir(file, tempDirArg);
        File tempPath = tempUploadToLocal(file, tempDirArg);
        // 将ID与文件存储在hadoop中所在的文件夹进行绑定，由本地上传至hadoop，最后删除本地临时目录
        String targetDir;
        if (!isEncrypt) {
            targetDir = HdfsConfig.basicDir + HdfsConfig.noEncryptionDir + HdfsConfig.noEncryptionDir + noEncyptFolderNumber;
        } else {
            targetDir = HdfsConfig.basicDir + HdfsConfig.encryptionDir + HdfsConfig.encryptionDir + encryptFolderNumber;
        }
        idBindDir(id, targetDir);
        String targetPath = targetDir + "/" + file.getOriginalFilename();
        hadoopUpload(URL, owner, bufferSize, tempPath, targetPath);
        deleteTempDir(tempDir);
        // 为上传的文件更名，放置重名文件出现覆盖现象，并将id与新的文件名的全路径进行绑定
        int fileCount = autoCreateDir(URL, owner, targetDir, isEncrypt);
        String newTargetPath = targetDir + "/" + "{" + fileCount + "}" + file.getOriginalFilename();
        FileRename.fileRename(URL, owner, targetPath, newTargetPath);
        idBindPath(id, newTargetPath);
        // mysql数据处理
        Long tempId = id;
        ID = tempId.toString();
        Long tempSize = new FileSizeService().fileSize(URL, owner, newTargetPath);
        size = tempSize.toString();
        time = new FileTimeService().fileTime(id);
        return id;
    }

}