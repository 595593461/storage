package com.example.storage.service;

import org.springframework.web.multipart.MultipartFile;

import java.io.*;

public interface UploadInterface {

    // ID绑定接收到的文件
    void idBindFile(long id, MultipartFile file);

    // ID绑定文件上传至hadoop后，所在的文件夹
    void idBindDir(long id, String targetDir);

    // ID绑定文件上传至hadoop后的全路径
    void idBindPath(long id, String targetPath);

    // 在hadoop创建父文件夹
    void createParentDir(String URL, String owner) throws Exception;

    // 在本地创建临时文件夹
    File createTempLocalDir(MultipartFile file, String tempDir);

    // 临时上传文件到本地临时文件夹
    File tempUploadToLocal(MultipartFile file, String tempDir) throws IOException;

    // 删除临时文件夹
    void deleteTempDir(File tempDir);

    // 当文件上传至hadoop中，自动在hadoop内创建文件夹，便于管理，提高效率
    int autoCreateDir(String URL, String owner, String targetDir, boolean isEncrypt) throws Exception;

    // 文件由本地临时文件夹内上传至hadoop
    void hadoopUpload(String URL, String owner, int bufferSize, File tempPath, String targetPath) throws Exception;
}