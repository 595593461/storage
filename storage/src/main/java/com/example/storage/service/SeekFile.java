package com.example.storage.service;

import com.example.storage.config.HdfsConfig;
import com.example.storage.config.HdfsInitialization;
import org.apache.commons.lang3.StringUtils;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.LocatedFileStatus;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.fs.RemoteIterator;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class SeekFile {
    public static List<Map<String, String>> SeekFile(String URL, String owner,Long id) throws Exception {

        FileSystem fs = HdfsInitialization.getFileSystem(URL, owner);
        String path =HdfsConfig.idBindPath.get(id);
        if (StringUtils.isEmpty(path)) {
            return null;
        }
        if (!fs.exists(new Path(path)))
            throw new IllegalArgumentException("文件不存在！");
        // 目标路径
        Path srcPath = new Path(path);
        // 递归找到所有文件
        RemoteIterator<LocatedFileStatus> filesList = fs.listFiles(srcPath, true);
        List<Map<String, String>> returnList = new ArrayList<>();
        while (filesList.hasNext()) {
            LocatedFileStatus next = filesList.next();
            String fileName = next.getPath().getName();
            Path filePath = next.getPath();
            Map<String, String> map = new HashMap<>();
            map.put("fileName", fileName);
            map.put("filePath", filePath.toString());
            returnList.add(map);
        }
        fs.close();
        return returnList;
    }
}




