package com.example.storage.controller;

import com.example.storage.config.HdfsConfig;
import com.example.storage.service.*;
import com.example.storage.service.mysqlservice.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/dev")
public class HdfsController {

    @Autowired
    private HdfsConfig hdfs;
    @Autowired
    private JdbcTemplate jdbcTemplate;

    @PostMapping("/noencryptupload")
    public long noEncryptUpload(@RequestParam(value = "noEncryptUpload", required = false) MultipartFile file) throws
            Exception {
        // 不加密文件上传至hadoop
        long id = new UploadService().upload(hdfs.getURL(), hdfs.getOwner(), hdfs.getBufferSize(), file, false, hdfs.getTempDir());
        new InsertService().insertToStorage(jdbcTemplate);
        return id;
    }

    @PostMapping("/encryptupload")
    public long encryptUpload(@RequestParam(value = "encryptUpload", required = false) MultipartFile file) throws
            Exception {
        // 加密文件上传至hadoop
        long id = new UploadService().upload(hdfs.getURL(), hdfs.getOwner(), hdfs.getBufferSize(), file, true, hdfs.getTempDir());
        new InsertService().insertToStorage(jdbcTemplate);
        return id;
    }

    @PostMapping("/download")
    public boolean download(@RequestParam(value = "id", required = false) Long id,
                            @RequestParam(value = "path", required = false) String downloadPath) throws Exception {
        return new DownloadService().download(hdfs.getURL(), hdfs.getOwner(), id, downloadPath);
    }

    @PostMapping("/delete")
    public boolean delete(@RequestParam(value = "id", required = false) Long id) throws Exception {
        new DelService().deleteToStorage(jdbcTemplate, id);
        return new DeleteService().delete(hdfs.getURL(), hdfs.getOwner(), id);
    }

    // 以下内容测试用
    @RequestMapping("/createtable")
    public void createTable() {
        new CreateTableService().creatStorageTable(jdbcTemplate);
        new CreateTableService().creatSecretKeyTable(jdbcTemplate);
    }

    @RequestMapping("/insertkey")
    public void test() {
        new InsertService().insertToKey(jdbcTemplate);
    }

    @RequestMapping("/selectstorage")
    public List<Map<String, Object>> Query() {
        String sql = "select * from storage";
        List<Map<String, Object>> userList = jdbcTemplate.queryForList(sql);
        return userList;
    }

    @PostMapping("/seek")
    public void seek(@RequestParam(value = "id", required = false) Long id) throws Exception {
        List<Map<String, String>> List = new SeekFile().SeekFile(hdfs.getURL(), hdfs.getOwner(), id);
        for (Map<String, String> map : List) {
            for (String key : map.keySet()) {
                System.out.println(key + " : " + map.get(key));
            }
        }
    }

    /*@RequestMapping("/filenum")
    public int fileNum() throws Exception {
        int num = new StatisticsService().fileNumber(hdfs.getURL(), hdfs.getOwner(), HdfsConfig.basicDir + HdfsConfig.noEncryptionDir + "1");
        return num;
    }*/

}